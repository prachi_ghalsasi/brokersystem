import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";
import { LivePriceService } from "../shared/live-price.service";

@Component({
  selector: 'app-stock-enlist',
  templateUrl: './stock-enlist.component.html',
  styleUrls: ['./stock-enlist.component.css']
})
export class StockEnlistComponent implements OnInit {

  Stocks: any = [];
  constructor( 
    public restApi: RestApiService,
    public livePriceService: LivePriceService
    ) { }

  ngOnInit(): void {
    this.loadStocks()
  }

  loadStocks() {
    return this.restApi.getStocks().subscribe((data: {}) => {
        console.log(data);
        this.Stocks = data;

        for(let stock of this.Stocks) {
            this.livePriceService.getPrices(stock.stock_ticker)
                                 .subscribe((data: any) => {
              console.log("price data");
              console.log(data);
              stock.last_close_price = (data.price_data[0][1]).toFixed(2);  
            });
        }
    })
  }


}
