import { ComponentFixture, TestBed } from '@angular/core/testing';


import { StockEnlistComponent } from './stock-enlist.component';

describe('StockEnlistComponent', () => {
  let component: StockEnlistComponent;
  let fixture: ComponentFixture<StockEnlistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StockEnlistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StockEnlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
