import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockCreateComponent } from './stock-create/stock-create.component';
import { StockListComponent } from './stock-list/stock-list.component';
import { StockEnlistComponent } from './stock-enlist/stock-enlist.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'stock-list' },
  { path: 'create-stock', component: StockCreateComponent },
  { path: 'stock-list', component: StockListComponent },
  { path: 'stock-enlist', component: StockEnlistComponent } 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
