import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { LivePriceService } from '../shared/live-price.service';
import { RestApiService } from "../shared/rest-api.service";
import { Stock } from '../shared/stock';

@Component({
  selector: 'app-stock-create',
  templateUrl: './stock-create.component.html',
  styleUrls: ['./stock-create.component.css']
})
export class StockCreateComponent implements OnInit {

  @Input() stockDetails: Stock = new Stock();

  constructor(
    public restApi: RestApiService, 
    public router: Router,
    public livePriceService: LivePriceService
    ) { }

  ngOnInit() { 
    
    // this.livePriceService.getPrices(stock.stock_ticker)
    //                              .subscribe((data: any) => {
    //           console.log("price data");
    //           console.log(data);
    //           stock.last_close_price = (data.price_data[0][1]).toFixed(2);  
    //         });
  }

  buyStock() {
    console.log(this.stockDetails.stock_ticker);
    console.log(this.stockDetails.price);
    this.stockDetails.buy_or_sell = "BUY";
    this.restApi.createStock(this.stockDetails).subscribe((data: {}) => {
      this.router.navigate(['/stock-list'])
    })
  }
  

  sellStock() {
    console.log(this.stockDetails.stock_ticker);
    console.log(this.stockDetails.price);
    this.stockDetails.buy_or_sell = "SELL";
    this.restApi.createStock(this.stockDetails).subscribe((data: {}) => {
      this.router.navigate(['/stock-list'])
    })
  }
  updatePrice(){
     this.livePriceService.getPrices(this.stockDetails.stock_ticker)
                                 .subscribe((data: any) => {
              console.log("price data");
              console.log(data);
              this.stockDetails.price = (data.price_data[0][1]).toFixed(2); 
              this.stockDetails.last_close_price = (data.price_data[0][1]).toFixed(2); 
            });
  }

}