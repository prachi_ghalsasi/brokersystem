import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";
import { LivePriceService } from "../shared/live-price.service";
import { Stock } from '../shared/stock';
@Component({
  selector: 'app-stock-list',
  templateUrl: './stock-list.component.html',
  styleUrls: ['./stock-list.component.css']
})
export class StockListComponent implements OnInit {

  Stocks: any = [];
  closeResult: string = "";
  constructor( 
    public restApi: RestApiService,
    public livePriceService: LivePriceService,
   
    ) { }

  ngOnInit(): void {
    this.loadStocks()
  }

  loadStocks() {
    return this.restApi.getStocks().subscribe((data: {}) => {
        console.log(data);
        this.Stocks = data;

        for(let stock of this.Stocks) {
            this.livePriceService.getPrices(stock.stock_ticker)
                                 .subscribe((data: any) => {
              console.log("price data");
              console.log(data);
              stock.last_close_price = (data.price_data[0][1]).toFixed(2);  
            });
        }
    })
  }

  deleteStock(id:any) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.restApi.deleteStock(id).subscribe(data => {
        this.loadStocks()
      })
    }
  } 
  
  updateStock(stock:Stock){
    let text ;
    let stock1 = new Stock();
    stock1.buy_or_sell = stock.buy_or_sell;
    stock1.stock_ticker = stock.stock_ticker;
    stock1.customer_id = stock.customer_id;
    stock1.status_code = 0 ;
    stock1.transaction_id = stock.transaction_id;
    let price:any = 0;
    let quantity = window.prompt("Quantity:");
    if(quantity == null)
      return ;
    stock1.quantity = parseInt(quantity) ;
    this.livePriceService.getPrices(stock1.stock_ticker)
                                 .subscribe((data: any) => {
               price = data.price_data[0][1].toFixed(2) ; 
            
      stock1.price = price ;
      this.restApi.updateStock(stock1).subscribe((data: any) => {
        console.log(data) ; 
    });
  });
  }
  
 



}
