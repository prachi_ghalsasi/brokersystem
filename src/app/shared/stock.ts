export class Stock {
    constructor(){
        this.transaction_id=0;
        this.stock_ticker="";
        this.buy_or_sell="";
        this.status_code=0;
        this.quantity=0;
        this.price=0;
        this.customer_id=1; // default for now
        this.last_close_price=0; // get this from price service

    }
    transaction_id: number;
    stock_ticker: string;
    buy_or_sell: string
    status_code: number;
    quantity: number;
    price: number;
    customer_id: number;
    last_close_price: number;

    
}